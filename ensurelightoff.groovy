/**
 *  Ensure Lights Off
 *
 *  Copyright 2017 David Dupplaw
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 */
definition(
    name: "Ensure Lights Off",
    namespace: "uk.me.dupplaw.david.st",
    author: "David Dupplaw",
    description: "If lights are turned on during certain time spans, they are automatically turned off again.  Useful for children who want to play toys all night.",
    category: "My Apps",
    iconUrl: "https://s3.amazonaws.com/smartapp-icons/Convenience/Cat-Convenience.png",
    iconX2Url: "https://s3.amazonaws.com/smartapp-icons/Convenience/Cat-Convenience@2x.png",
    iconX3Url: "https://s3.amazonaws.com/smartapp-icons/Convenience/Cat-Convenience@2x.png") {
}


preferences {
	section("Light to check") {
    	input "theLight", "capability.switch", title: "Which light?", required: true
	}
    section("Times to check") {
    	input "startTime", "time", title: "From:", required: true
        input "endTime", "time", title: "To:", required: true
    }
}

def installed() {
	log.debug "Installed with settings: ${settings}"

	initialize()
}

def updated() {
	log.debug "Updated with settings: ${settings}"

	unsubscribe()
	initialize()
}

def initialize() {
	subscribe(theLight, "switch.on", lightOnHandler)
}

def lightOnHandler(evt) {
    def startDate = timeToday(startTime)
    def endDate   = timeToday(endTime);
    def timeZone  = location.getTimeZone()
    
    if( timeZone == null ) {
    	timeZone = TimeZone.getDefault();
    }
    
    def isTime = timeOfDayIsBetween(startDate, endDate, new Date(), timeZone )
    if( isTime ) {
		lightOff();
    	log.debug "Light was turned on at ${new Date()} - turned it off again";
        
        runIn( 1, lightOff );
        runIn( 5, lightOff );
        runIn( 10, lightOff );
    }
}

def lightOff() {
	theLight.off();
}

